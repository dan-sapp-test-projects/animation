const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const extractSass = new ExtractTextPlugin({
  filename: 'styles.css'
});

module.exports = {
  entry: path.resolve(__dirname, 'src/js/index.jsx'),
  output: {
    path: path.resolve(__dirname, './dist/'),
    publicPath: '/dist/',
    filename: 'main.js'
  },
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: [path.resolve(__dirname, 'src/js'), path.resolve(__dirname, 'node_modules/@hppl')],
        loader: 'babel-loader',
        options: {
          babelrc: true,
          plugins: ['transform-class-properties', 'transform-es2015-modules-commonjs']
        }
      },
      {
        test: /\.s?css$/,
        use: extractSass.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true,
                sourceMap: true
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true
              }
            }
          ],
          fallback: 'style-loader'
        })
      }
    ]
  },
  plugins: [
    extractSass,
    new CopyWebpackPlugin([
      { context: 'public', from: '**/*', to: '.' }
    ], { copyUnmodified: true }),
    new HtmlWebpackPlugin({
      template: './index.html',
      minify: false,
      title: 'Connect Four'
    })
  ],
  resolve: {
    modules: ['node_modules', path.resolve(__dirname, 'src/js')],
    extensions: ['.js', '.jsx', '.scss', '.css'],
    symlinks: false
  },

  devServer: {
    historyApiFallback: true,
    port: 3000
  },
  devtool: 'inline-source-map',
  context: __dirname,
  target: 'web'
};