const config = require('./webpack.config');
const path = require('path');

config.devtool = false;
config.entry = ['./src/js/index.jsx'];
config.output = {
  path: path.resolve(__dirname, './dist/animation'),
  filename: 'main.js',
  library: 'animation',
  libraryTarget: 'umd',
  umdNamedDefine: true
};
module.exports = config;