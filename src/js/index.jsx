import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';

const InitApp = () => (
  <App />
);

ReactDOM.render(<InitApp />, document.getElementById('animation-app'));
