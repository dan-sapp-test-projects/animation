import React from 'react'
import Default from './default'
import Principle1 from './principle1'
import Principle2 from './principle2'

const App = () => {
    return (
        <div>
            <Default/>
            <Principle1/>
            <Principle2/>
        </div>
    )
}

export default App