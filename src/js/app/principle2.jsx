import React, {useState, useEffect} from 'react'
import styled from 'styled-components'

const Frame = styled.div`
    border: 1px solid black;
    height: 150px;
    width: 500px;
    position: relative;
`
const Ball = styled.div`
    background: red;
    height: 100px;
    width: 100px;
    border-radius: 50px;
    margin-top: 25px;
    position: absolute;
    &.move-right {
        animation: to-right2 800ms linear;
        left: 400px;
        @keyframes to-right2 {
            0%   {
                left: 0px;
                height: 100px;
                width: 100px;
                border-radius: 50px;
                margin-top: 25px;
            }
            10%   {
                left: 0px;
                height: 140px;
                width: 60px;
                border-radius: 70px;
                margin-top: 5px;
            }
            20%  {
                left: 0px;
                height: 100px;
                width: 100px;
                border-radius: 50px;
                margin-top: 25px;
            }
            30%  {
                left: 60px;
                height: 95px;
                width: 105px;
                border-radius: 50px;
                margin-top: 30px;
            }
            40%  {
                left: 120px;
                height: 90px;
                width: 110px;
                border-radius: 60px;
                margin-top: 30px;
            }
            60%  {
                left: 240px;
                height: 90px;
                width: 110px;
                border-radius: 60px;
                margin-top: 30px;
            }
            80%  {
                left: 380px;
                height: 90px;
                width: 110px;
                border-radius: 60px;
                margin-top: 30px;
            }
            90%  {
                left: 420px;
                height: 120px;
                width: 80px;
                border-radius: 60px;
                margin-top: 15px;
            }
            100% {
                left: 400px;
                height: 100px;
                width: 100px;
                border-radius: 50px;
                margin-top: 25px;
            }
        }
    }
    &.move-left {
        left: 0;
        animation: to-left2 800ms linear;
        @keyframes to-left2 {
            0%   {
                left: 400px;
                height: 100px;
                width: 100px;
                border-radius: 50px;
                margin-top: 25px;
            }
            10%   {
                left: 440px;
                height: 140px;
                width: 60px;
                border-radius: 70px;
                margin-top: 5px;
            }
            20%  {
                left: 400px;
                height: 100px;
                width: 100px;
                border-radius: 50px;
                margin-top: 25px;
            }
            40%  {
                left: 280px;
                height: 90px;
                width: 110px;
                border-radius: 50px;
                margin-top: 30px;
            }
            60%  {
                left: 140px;
                height: 90px;
                width: 110px;
                border-radius: 50px;
                margin-top: 30px;
            }
            80%  {
                left: 0px;
                height: 90px;
                width: 110px;
                border-radius: 50px;
                margin-top: 30px;
            }
            90%  {
                left: 0px;
                height: 120px;
                width: 80px;
                border-radius: 60px;
                margin-top: 15px;
            }
            100% {
                left: 0px;
                height: 100px;
                width: 100px;
                border-radius: 50px;
                margin-top: 25px;
            }
        }
    }
`

const Principle2 = () => {
    let animClass ='squash'
    const [seconds, setSeconds] = useState(0);
    const stageFunc = (num) => { return num % 2;}
    const speed = 2000
    useEffect(() => {
        const interval = setInterval(() => {
            setSeconds(seconds => seconds + 1);
        }, speed);
        return () => clearInterval(interval);
    }, []);
    const stage = stageFunc(seconds)
    switch(stage) {
        case 0:
            animClass = 'move-right'
            break;
        case 1:
            animClass = 'move-left'
            break;
        default:
    }
    return (
        <div>
            <h2>Principle 2: Anticipation</h2>
            <Frame>
                <Ball className={animClass}/>
            </Frame>
        </div>
    )
}

export default Principle2