import React, {useState, useEffect} from 'react'
import styled from 'styled-components'

const Frame = styled.div`
    border: 1px solid black;
    height: 150px;
    width: 500px;
    position: relative;
`
const Ball = styled.div`
    background: red;
    height: 100px;
    width: 100px;
    border-radius: 50px;
    margin-top: 25px;
    position: absolute;
    &.move-right {
        animation: to-right0 800ms linear;
        left: 400px;
        height: 100px;
        width: 100px;
        border-radius: 50px;
        margin-top: 25px;
        @keyframes to-right0 {
            0%   {
                left: 0px;
            }
            20%   {
                left: 0px;
            }
            80% {
                left: 400px;
            }
            100% {
                left: 400px;
            }
        }
    }
    &.move-left {
        left: 0;
        height: 100px;
        width: 100px;
        border-radius: 50px;
        margin-top: 25px;
        animation: to-left0 800ms linear;
        @keyframes to-left0 {
            0%   {
                left: 400px;
            }
            20%   {
                left: 400px;
            }
            80% {
                left: 0px;
            }
            100% {
                left: 0px;
            }
        }
    }
`

const Default = () => {
    let animClass ='squash'
    const [seconds, setSeconds] = useState(0);
    const stageFunc = (num) => { return num % 2;}
    const speed = 2000
    useEffect(() => {
        const interval = setInterval(() => {
            setSeconds(seconds => seconds + 1);
        }, speed);
        return () => clearInterval(interval);
    }, []);
    const stage = stageFunc(seconds)
    switch(stage) {
        case 0:
            animClass = 'move-right'
            break;
        case 1:
            animClass = 'move-left'
            break;
        default:
    }
    return (
        <div>
            <h2>Default</h2>
            <Frame>
                <Ball className={animClass}/>
            </Frame>
        </div>
    )
}

export default Default